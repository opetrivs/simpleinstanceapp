package com.binariks.simpleinstanceapp.feature;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private ArrayList<String> urlsList;

    private WebView webView;
    private Button btnLeft, btnRight;
    private FrameLayout progressContainer;
    private int currentUrlIndex = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initAndSetupList();
        initViews();
        setupViews();

    }

    private void initAndSetupList() {
        urlsList = new ArrayList<>();
        urlsList.add("https://mail.google.com");
        urlsList.add("https://www.google.com");
        urlsList.add("https://www.yahoo.com");
    }

    private void setupViews() {
        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                Log.d(TAG, "onProgressChanged: progress = " + newProgress);
                if (newProgress == 100)
                    hideProgress();
            }
        });

        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            }
        });

        loadWebViewUrl();

        btnRight.setOnClickListener(this);
        btnLeft.setOnClickListener(this);

    }

    private void hideProgress() {
        setButtonsClickable(true);
        progressContainer.setVisibility(View.GONE);
    }

    private void showProgress(){
        setButtonsClickable(false);
        progressContainer.setVisibility(View.VISIBLE);
    }

    private void setButtonsClickable(boolean clickable) {
        btnLeft.setClickable(clickable);
        btnRight.setClickable(clickable);
    }

    private void initViews() {
        webView = findViewById(R.id.web_view);
        btnLeft = findViewById(R.id.btn_left_arrow);
        btnRight = findViewById(R.id.btn_right_arrow);
        progressContainer = findViewById(R.id.progress_container);
    }

    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_left_arrow) {
            Log.d(TAG, "onClick: btn_left_arrow");
            if (currentUrlIndex - 1 >= 0) {
                currentUrlIndex--;
                loadWebViewUrl();
            }

        } else if (id == R.id.btn_right_arrow) {
            Log.d(TAG, "onClick: btn_right_arrow");
            if (currentUrlIndex + 1 < urlsList.size()) {
                currentUrlIndex++;
                loadWebViewUrl();
            }
        }
    }

    private void loadWebViewUrl() {
        showProgress();
        webView.loadUrl(urlsList.get(currentUrlIndex));
    }
}
